
#ifndef _SPRING_H_
#define _SPRING_H_

struct Spring
{
	Spring(int _p1, int _p2, float _stiff, float _initLength);
	~Spring();

	int massPoint1;
	int massPoint2;
	float stiffness; 
	float initialLength;
};

#endif //_SPRING_H_