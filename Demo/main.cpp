//--------------------------------------------------------------------------------------
// File: main.cpp
//
// The main file containing the entry point main().
//--------------------------------------------------------------------------------------

#include <sstream>
#include <iomanip>
#include <random>
#define _USE_MATH_DEFINES
#include <cmath>
#include <iostream>

//DirectX includes
#include <DirectXMath.h>
using namespace DirectX;
using std::cout;

// Effect framework includes
#include <d3dx11effect.h>

// DXUT includes
#include <DXUT.h>
#include <DXUTcamera.h>

// DirectXTK includes
#include "Effects.h"
#include "VertexTypes.h"
#include "PrimitiveBatch.h"
#include "GeometricPrimitive.h"
#include "ScreenGrab.h"

// AntTweakBar includes
#include "AntTweakBar.h"

// Internal includes
#include "util/util.h"
#include "util/FFmpeg.h"

//#define MASS_SPRING_SYSTEM
#define RIGID_BODY_SYSTEM

#ifdef MASS_SPRING_SYSTEM
#include "MassSpringSystem.h"
#include "MassPoint.h"
#include "Spring.h"
#endif
#ifdef RIGID_BODY_SYSTEM
#include "RigidBoxSystem.h"
#endif

// DXUT camera
// NOTE: CModelViewerCamera does not only manage the standard view transformation/camera position 
//       (CModelViewerCamera::GetViewMatrix()), but also allows for model rotation
//       (CModelViewerCamera::GetWorldMatrix()). 
//       Look out for CModelViewerCamera::SetButtonMasks(...).
CModelViewerCamera g_camera;

// Effect corresponding to "effect.fx"
ID3DX11Effect* g_pEffect = nullptr;
ID3D11Device* g_pPd3Device = nullptr;
// Main tweak bar
TwBar* g_pTweakBar;

// DirectXTK effects, input layouts and primitive batches for different vertex types
// (for drawing multicolored & unlit primitives)
BasicEffect*                          g_pEffectPositionColor          = nullptr;
ID3D11InputLayout*                    g_pInputLayoutPositionColor     = nullptr;
PrimitiveBatch<VertexPositionColor>*  g_pPrimitiveBatchPositionColor  = nullptr;

// DirectXTK effect, input layout and primitive batch for position/normal vertices
// (for drawing unicolor & oriented & lit primitives)
BasicEffect*                          g_pEffectPositionNormal         = nullptr;
ID3D11InputLayout*                    g_pInputLayoutPositionNormal    = nullptr;
PrimitiveBatch<VertexPositionNormal>* g_pPrimitiveBatchPositionNormal = nullptr;

BasicEffect*                               g_pEffectPositionNormalColor         = nullptr;
ID3D11InputLayout*                         g_pInputLayoutPositionNormalColor    = nullptr;
PrimitiveBatch<VertexPositionNormalColor>* g_pPrimitiveBatchPositionNormalColor = nullptr;

// DirectXTK simple geometric primitives
std::unique_ptr<GeometricPrimitive> g_pSphere;
std::unique_ptr<GeometricPrimitive> g_pTeapot;
std::unique_ptr<GeometricPrimitive> g_pCube;

// Movable object management
XMINT2   g_viMouseDelta = XMINT2(0, 0);
XMFLOAT3 g_vfMovableObjectVel = XMFLOAT3(0, 0, 0);
XMFLOAT3 g_vfMovableObjectPos = XMFLOAT3(0, 0, 0);
XMFLOAT3 g_vfRotate = XMFLOAT3(0, 0, 0);

// TweakAntBar GUI variables

int g_iTestCase = 0;
int g_iPreTestCase = -1;
bool  g_bSimulateByStep = false;
bool  g_bIsSpaceReleased = true;

#ifdef MASS_SPRING_SYSTEM
TwEnumVal integratorEV[] = { { Integrator::Euler, "Euler" }, { Integrator::Midpoint, "Midpoint" }, { Integrator::Leap_Frog, "Leap Frog" } };

MassSpringSystem massSpringSystem;
double lastStep = 0;

float g_fStepSize = 0.005f;
float g_fTimeScale = 1;
TwType g_eIntegrator;
float g_fSphereSize = 0.03f;
bool g_bDrawPoints = true;
bool g_bDrawSprings = true;
bool g_bDrawVelocities = false;

XMFLOAT3 initPosition1 = XMFLOAT3(0.0, 0.0, 0.0);
XMFLOAT3 initPosition2 = XMFLOAT3(0.0, 2.0, 0.0);
XMFLOAT3 initVelocity1 = XMFLOAT3(-1.0, 0.0, 0.0);
XMFLOAT3 initVelocity2 = XMFLOAT3(1.0, 0.0, 0.0);

static std::vector<MassPoint> scene1Points = { MassPoint(initPosition1, initVelocity1, 10.0f), 
												MassPoint(initPosition2, initVelocity2, 10.0f) };
static std::vector<Spring> scene1Springs = { Spring(0, 1, 40.0f, 1.0f) };

static std::vector<MassPoint> scene2Points = {
	MassPoint(0.5f, 1, 0.5f, 1),	//0
	MassPoint(0.5f, 1, -0.5f, 1),	//1
	MassPoint(-0.5f, 1, -0.5f, 1),	//2
	MassPoint(-0.5f, 1, 0.5f, 1),	//3
	MassPoint(0.5f, 2, 0.5f, 1),	//4
	MassPoint(0.5f, 2, -0.5f, 1),	//5
	MassPoint(-0.5f, 2, -0.5f, 1),	//6
	MassPoint(-0.5f, 2, 0.5f, 1),	//7
	MassPoint(-0.5f, 5, -0.5f, 1, true),	//8
	MassPoint(0.5f, 5, 0.5f, 1, true),	//9
};

float stiffness = 100.0f;
static std::vector<Spring> scene2Springs = {
	Spring(0, 1, stiffness, 1.0f),
	Spring(0, 2, stiffness, 1.41421f),
	Spring(0, 3, stiffness, 1.0f),
	Spring(0, 4, stiffness, 1.0f),
	Spring(0, 5, stiffness, 1.41421f),
	Spring(0, 6, stiffness, 1.73205f),
	Spring(0, 7, stiffness, 1.41421f),
	Spring(1, 2, stiffness, 1.0f),
	Spring(1, 3, stiffness, 1.41421f),
	Spring(1, 4, stiffness, 1.41421f),
	Spring(1, 5, stiffness, 1.0f),
	Spring(1, 6, stiffness, 1.41421f),
	Spring(1, 7, stiffness, 1.73205f),
	Spring(2, 3, stiffness, 1.0f),
	Spring(2, 4, stiffness, 1.73205f),
	Spring(2, 5, stiffness, 1.41421f),
	Spring(2, 6, stiffness, 1.0f),
	Spring(2, 7, stiffness, 1.41421f),
	Spring(3, 4, stiffness, 1.41421f),
	Spring(3, 5, stiffness, 1.73205f),
	Spring(3, 6, stiffness, 1.41421f),
	Spring(3, 7, stiffness, 1.0f),
	Spring(4, 5, stiffness, 1.0f),
	Spring(4, 6, stiffness, 1.41421f),
	Spring(4, 7, stiffness, 1.0f),
	Spring(5, 6, stiffness, 1.0f),
	Spring(5, 7, stiffness, 1.41421f),
	Spring(6, 7, stiffness, 1.0f),
	Spring(8, 6, stiffness / 2.0f, 3.0f),
	Spring(9, 4, stiffness / 2.0f, 3.0f),
};

#endif
#ifdef RIGID_BODY_SYSTEM
double lastStep = 0;
float g_fStepSize = 0.005f;
float g_fTimeScale = 1;
RigidBoxSystem rigidBoxSystem;
RigidBox box1 = RigidBox(2.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.6f, 0.5f, 0.0f, 0.0f, DirectX::XMConvertToRadians(90.0f));
RigidBox box2 = RigidBox(2.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.6f, 0.5f, 0.0f, 0.0f, 0.0f);
RigidBox box3 = RigidBox(2.0f, -1.0f, 0.0f, 0.0f, 1.0f, 0.6f, 0.5f, 0.0f, 
	DirectX::XMConvertToRadians(35.0f), DirectX::XMConvertToRadians(35.0f));
RigidBox box4 = RigidBox(2.0f, 2.0f, 0.0f, 0.0f, 1.0f, 0.6f, 0.5f, 0.0f, 0.0f, DirectX::XMConvertToRadians(90.0f));
RigidBox box5 = RigidBox(2.0f, 0.0f, 1.5f, 0.0f, 1.0f, 0.6f, 0.5f, 0.0f, 
	0.0f, DirectX::XMConvertToRadians(90.0f));
RigidBox boxFloor = RigidBox(0.0f, 0.0f, -1.5f, 0.0f, 6.0f, 1.0f, 6.0f);
#endif


// Video recorder
FFmpeg* g_pFFmpegVideoRecorder = nullptr;

// Create TweakBar and add required buttons and variables
void InitTweakBar(ID3D11Device* pd3dDevice)
{
    g_pTweakBar = TwNewBar("TweakBar");
	TwDefine(" TweakBar color='0 128 128' alpha=128 ");

#ifdef MASS_SPRING_SYSTEM
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Test Scene", "Console, 2 Points, Cube");
#endif
#ifdef RIGID_BODY_SYSTEM
	TwType TW_TYPE_TESTCASE = TwDefineEnumFromString("Test Scene", "Console, One box, Two boxes, Four boxes");
#endif
	TwAddVarRW(g_pTweakBar, "Test Scene", TW_TYPE_TESTCASE, &g_iTestCase, "");
	// HINT: For buttons you can directly pass the callback function as a lambda expression.
	TwAddButton(g_pTweakBar, "Reset Scene", [](void *){g_iPreTestCase = -1; }, nullptr, "");
	TwAddButton(g_pTweakBar, "Reset Camera", [](void *){g_camera.Reset(); }, nullptr, "");
	// Run mode, step by step, control by space key
	TwAddVarRW(g_pTweakBar, "RunStep(space)", TW_TYPE_BOOLCPP, &g_bSimulateByStep, "");

#ifdef MASS_SPRING_SYSTEM
	g_eIntegrator = TwDefineEnum("IntegratorType", integratorEV, 3);

	TwAddSeparator(g_pTweakBar, "", NULL);
	TwAddVarRW(g_pTweakBar, "Step Size", TW_TYPE_FLOAT, &g_fStepSize, "min=0.001 max=0.1 step=0.001");
	TwAddVarRW(g_pTweakBar, "Time Scale", TW_TYPE_FLOAT, &g_fTimeScale, "min=0.001 max=2 step=0.001");
	TwAddVarRW(g_pTweakBar, "Integrator", g_eIntegrator, &massSpringSystem.integrator, NULL);
	TwAddSeparator(g_pTweakBar, "", NULL);
	TwAddVarRW(g_pTweakBar, "Draw Points", TW_TYPE_BOOLCPP, &g_bDrawPoints, "");
	TwAddVarRW(g_pTweakBar, "Draw Springs", TW_TYPE_BOOLCPP, &g_bDrawSprings, "");
	TwAddVarRW(g_pTweakBar, "Draw Velocities", TW_TYPE_BOOLCPP, &g_bDrawVelocities, "");
#endif
#ifdef RIGID_BODY_SYSTEM
	TwAddSeparator(g_pTweakBar, "", NULL);
	TwAddVarRW(g_pTweakBar, "Step Size", TW_TYPE_FLOAT, &g_fStepSize, "min=0.001 max=0.1 step=0.001");
	TwAddVarRW(g_pTweakBar, "Time Scale", TW_TYPE_FLOAT, &g_fTimeScale, "min=0.001 max=5 step=0.001");
#endif
}

// Draw the edges of the bounding box [-0.5;0.5]� rotated with the cameras model tranformation.
// (Drawn as line primitives using a DirectXTK primitive batch)
void DrawBoundingBox(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/color effect
    g_pEffectPositionColor->SetWorld(g_camera.GetWorldMatrix());
    
    g_pEffectPositionColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

    // Draw
    g_pPrimitiveBatchPositionColor->Begin();
    
    // Lines in x direction (red color)
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet(-0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red),
            VertexPositionColor(XMVectorSet( 0.5f, (float)(i%2)-0.5f, (float)(i/2)-0.5f, 1), Colors::Red)
        );
    }

    // Lines in y direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, -0.5f, (float)(i/2)-0.5f, 1), Colors::Green),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f,  0.5f, (float)(i/2)-0.5f, 1), Colors::Green)
        );
    }

    // Lines in z direction
    for (int i=0; i<4; i++)
    {
        g_pPrimitiveBatchPositionColor->DrawLine(
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f, -0.5f, 1), Colors::Blue),
            VertexPositionColor(XMVectorSet((float)(i%2)-0.5f, (float)(i/2)-0.5f,  0.5f, 1), Colors::Blue)
        );
    }

    g_pPrimitiveBatchPositionColor->End();
}

// Draw a large, square plane at y=-1 with a checkerboard pattern
// (Drawn as multiple quads, i.e. triangle strips, using a DirectXTK primitive batch)
void DrawFloor(ID3D11DeviceContext* pd3dImmediateContext)
{
    // Setup position/normal/color effect
    g_pEffectPositionNormalColor->SetWorld(XMMatrixIdentity());
    g_pEffectPositionNormalColor->SetEmissiveColor(Colors::Black);
    g_pEffectPositionNormalColor->SetDiffuseColor(0.8f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularColor(0.4f * Colors::White);
    g_pEffectPositionNormalColor->SetSpecularPower(1000);
    
    g_pEffectPositionNormalColor->Apply(pd3dImmediateContext);
    pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionNormalColor);

    // Draw 4*n*n quads spanning x = [-n;n], y = -1, z = [-n;n]
    const float n = 4;
    XMVECTOR normal      = XMVectorSet(0, 1,0,0);
    XMVECTOR planecenter = XMVectorSet(0,-1,0,0);

    g_pPrimitiveBatchPositionNormalColor->Begin();
    for (float z = -n; z < n; z++)
    {
        for (float x = -n; x < n; x++)
        {
            // Quad vertex positions
            XMVECTOR pos[] = { XMVectorSet(x  , -1, z+1, 0),
                               XMVectorSet(x+1, -1, z+1, 0),
                               XMVectorSet(x+1, -1, z  , 0),
                               XMVectorSet(x  , -1, z  , 0) };

            // Color checkerboard pattern (white & gray)
            XMVECTOR color = ((int(z + x) % 2) == 0) ? XMVectorSet(1,1,1,1) : XMVectorSet(0.6f,0.6f,0.6f,1);

            // Color attenuation based on distance to plane center
            float attenuation[] = {
                1.0f - XMVectorGetX(XMVector3Length(pos[0] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[1] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[2] - planecenter)) / n,
                1.0f - XMVectorGetX(XMVector3Length(pos[3] - planecenter)) / n };

            g_pPrimitiveBatchPositionNormalColor->DrawQuad(
                VertexPositionNormalColor(pos[0], normal, attenuation[0] * color),
                VertexPositionNormalColor(pos[1], normal, attenuation[1] * color),
                VertexPositionNormalColor(pos[2], normal, attenuation[2] * color),
                VertexPositionNormalColor(pos[3], normal, attenuation[3] * color)
            );
        }
    }
    g_pPrimitiveBatchPositionNormalColor->End();    
}

#ifdef MASS_SPRING_SYSTEM
void DrawMassSpringSystem(ID3D11DeviceContext* pd3dImmediateContext)
{

	if (g_bDrawPoints)
	{
		g_pEffectPositionColor->Apply(pd3dImmediateContext);
		pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

		g_pPrimitiveBatchPositionColor->Begin();

		for (MassPoint p : massSpringSystem.points)
		{
			if (p.isFixed) g_pEffectPositionNormal->SetDiffuseColor(Colors::Red);
			else g_pEffectPositionNormal->SetDiffuseColor(Colors::Orange);
			XMMATRIX scale = XMMatrixScaling(g_fSphereSize, g_fSphereSize, g_fSphereSize);
			XMMATRIX trans = XMMatrixTranslation(p.position.x, p.position.y, p.position.z);
			g_pEffectPositionNormal->SetWorld(scale * trans * XMMatrixIdentity());

			g_pSphere->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
		}

		g_pPrimitiveBatchPositionColor->End();
	}


	if (g_bDrawSprings)
	{
		g_pEffectPositionColor->SetWorld(XMMatrixIdentity());

		g_pEffectPositionColor->Apply(pd3dImmediateContext);
		pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

		g_pPrimitiveBatchPositionColor->Begin();

		for (Spring s : massSpringSystem.springs)
		{
			g_pPrimitiveBatchPositionColor->DrawLine(
				VertexPositionColor(XMLoadFloat3(&massSpringSystem.points[s.massPoint1].position), Colors::Blue),
				VertexPositionColor(XMLoadFloat3(&massSpringSystem.points[s.massPoint2].position), Colors::Blue)
			);
		}

		g_pPrimitiveBatchPositionColor->End();
	}

	if (g_bDrawVelocities)
	{
		g_pEffectPositionColor->SetWorld(XMMatrixIdentity());

		g_pEffectPositionColor->Apply(pd3dImmediateContext);
		pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

		g_pPrimitiveBatchPositionColor->Begin();

		for (MassPoint p : massSpringSystem.points)
		{
			if (p.isFixed)
				continue;

			XMVECTOR start = XMLoadFloat3(&p.position);
			XMVECTOR end = XMLoadFloat3(&p.position);
			XMVECTOR vel = XMLoadFloat3(&p.velocity);
			end = XMVectorAdd(end, vel);

			g_pPrimitiveBatchPositionColor->DrawLine(
				VertexPositionColor(start, Colors::Red),
				VertexPositionColor(end, Colors::Red)
			);
		}

		g_pPrimitiveBatchPositionColor->End();
	}
}
#endif
#ifdef RIGID_BODY_SYSTEM
void DrawRigidBodySystem(ID3D11DeviceContext* pd3dImmediateContext)
{
	g_pEffectPositionColor->Apply(pd3dImmediateContext);
	pd3dImmediateContext->IASetInputLayout(g_pInputLayoutPositionColor);

	g_pPrimitiveBatchPositionColor->Begin();

	for (int i = 0; i < rigidBoxSystem.boxes.size(); i++)
	{
		if (!rigidBoxSystem.boxes[i].visible)
			continue;
		g_pEffectPositionNormal->SetDiffuseColor(Colors::Orange);
		if((g_iTestCase == 1 || g_iTestCase == 3) && i == 0)
			g_pEffectPositionNormal->SetDiffuseColor(Colors::Red);
		g_pEffectPositionNormal->SetWorld(XMLoadFloat4x4(&rigidBoxSystem.boxes[i].transformMatrix()));

		g_pCube->Draw(g_pEffectPositionNormal, g_pInputLayoutPositionNormal);
	}

	g_pPrimitiveBatchPositionColor->End();
}
#endif
// ============================================================
// DXUT Callbacks
// ============================================================


//--------------------------------------------------------------------------------------
// Reject any D3D11 devices that aren't acceptable by returning false
//--------------------------------------------------------------------------------------
bool CALLBACK IsD3D11DeviceAcceptable( const CD3D11EnumAdapterInfo *AdapterInfo, UINT Output, const CD3D11EnumDeviceInfo *DeviceInfo,
                                       DXGI_FORMAT BackBufferFormat, bool bWindowed, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Called right before creating a device, allowing the app to modify the device settings as needed
//--------------------------------------------------------------------------------------
bool CALLBACK ModifyDeviceSettings( DXUTDeviceSettings* pDeviceSettings, void* pUserContext )
{
	return true;
}


//--------------------------------------------------------------------------------------
// Create any D3D11 resources that aren't dependent on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11CreateDevice( ID3D11Device* pd3dDevice, const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
	HRESULT hr;

    ID3D11DeviceContext* pd3dImmediateContext = DXUTGetD3D11DeviceContext();;

    std::wcout << L"Device: " << DXUTGetDeviceStats() << std::endl;
    
    // Load custom effect from "effect.fxo" (compiled "effect.fx")
	std::wstring effectPath = GetExePath() + L"effect.fxo";
	if(FAILED(hr = D3DX11CreateEffectFromFile(effectPath.c_str(), 0, pd3dDevice, &g_pEffect)))
	{
        std::wcout << L"Failed creating effect with error code " << int(hr) << std::endl;
		return hr;
	}

    // Init AntTweakBar GUI
	TwInit(TW_DIRECT3D11, pd3dDevice);
    InitTweakBar(pd3dDevice);

    // Create DirectXTK geometric primitives for later usage
    g_pSphere = GeometricPrimitive::CreateGeoSphere(pd3dImmediateContext, 2.0f, 2, false);
    g_pTeapot = GeometricPrimitive::CreateTeapot(pd3dImmediateContext, 1.5f, 8, false);
	g_pCube = GeometricPrimitive::CreateCube(pd3dImmediateContext, 1.0f, false);

    // Create effect, input layout and primitive batch for position/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionColor->SetVertexColorEnabled(true); // triggers usage of position/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);
        
        pd3dDevice->CreateInputLayout(VertexPositionColor::InputElements,
                                      VertexPositionColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionColor);

        // Primitive batch
        g_pPrimitiveBatchPositionColor = new PrimitiveBatch<VertexPositionColor>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormal = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormal->EnableDefaultLighting(); // triggers usage of position/normal vertices
        g_pEffectPositionNormal->SetPerPixelLighting(true);

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormal->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormal::InputElements,
                                      VertexPositionNormal::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormal);

        // Primitive batch
        g_pPrimitiveBatchPositionNormal = new PrimitiveBatch<VertexPositionNormal>(pd3dImmediateContext);
    }

    // Create effect, input layout and primitive batch for position/normal/color vertices (DirectXTK)
    {
        // Effect
        g_pEffectPositionNormalColor = new BasicEffect(pd3dDevice);
        g_pEffectPositionNormalColor->SetPerPixelLighting(true);
        g_pEffectPositionNormalColor->EnableDefaultLighting();     // triggers usage of position/normal/color vertices
        g_pEffectPositionNormalColor->SetVertexColorEnabled(true); // triggers usage of position/normal/color vertices

        // Input layout
        void const* shaderByteCode;
        size_t byteCodeLength;
        g_pEffectPositionNormalColor->GetVertexShaderBytecode(&shaderByteCode, &byteCodeLength);

        pd3dDevice->CreateInputLayout(VertexPositionNormalColor::InputElements,
                                      VertexPositionNormalColor::InputElementCount,
                                      shaderByteCode, byteCodeLength,
                                      &g_pInputLayoutPositionNormalColor);

        // Primitive batch
        g_pPrimitiveBatchPositionNormalColor = new PrimitiveBatch<VertexPositionNormalColor>(pd3dImmediateContext);
    }

	g_pPd3Device = pd3dDevice;
	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11CreateDevice 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11DestroyDevice( void* pUserContext )
{
	SAFE_RELEASE(g_pEffect);
	
    TwDeleteBar(g_pTweakBar);
    g_pTweakBar = nullptr;
	TwTerminate();

    g_pSphere.reset();
    g_pTeapot.reset();
	g_pCube.reset();
    
    SAFE_DELETE (g_pPrimitiveBatchPositionColor);
    SAFE_RELEASE(g_pInputLayoutPositionColor);
    SAFE_DELETE (g_pEffectPositionColor);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormal);
    SAFE_RELEASE(g_pInputLayoutPositionNormal);
    SAFE_DELETE (g_pEffectPositionNormal);

    SAFE_DELETE (g_pPrimitiveBatchPositionNormalColor);
    SAFE_RELEASE(g_pInputLayoutPositionNormalColor);
    SAFE_DELETE (g_pEffectPositionNormalColor);
}

//--------------------------------------------------------------------------------------
// Create any D3D11 resources that depend on the back buffer
//--------------------------------------------------------------------------------------
HRESULT CALLBACK OnD3D11ResizedSwapChain( ID3D11Device* pd3dDevice, IDXGISwapChain* pSwapChain,
                                          const DXGI_SURFACE_DESC* pBackBufferSurfaceDesc, void* pUserContext )
{
    // Update camera parameters
	int width = pBackBufferSurfaceDesc->Width;
	int height = pBackBufferSurfaceDesc->Height;
	g_camera.SetWindow(width, height);
	g_camera.SetProjParams(XM_PI / 4.0f, float(width) / float(height), 0.1f, 100.0f);

    // Inform AntTweakBar about back buffer resolution change
  	TwWindowSize(pBackBufferSurfaceDesc->Width, pBackBufferSurfaceDesc->Height);

	return S_OK;
}

//--------------------------------------------------------------------------------------
// Release D3D11 resources created in OnD3D11ResizedSwapChain 
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11ReleasingSwapChain( void* pUserContext )
{
}

//--------------------------------------------------------------------------------------
// Handle key presses
//--------------------------------------------------------------------------------------
void CALLBACK OnKeyboard( UINT nChar, bool bKeyDown, bool bAltDown, void* pUserContext )
{
    HRESULT hr;

	if(bKeyDown)
	{
		switch(nChar)
		{
            // RETURN: toggle fullscreen
			case VK_RETURN :
			{
				if(bAltDown) DXUTToggleFullScreen();
				break;
			}
            // F8: Take screenshot
			case VK_F8:
			{
                // Save current render target as png
                static int nr = 0;
				std::wstringstream ss;
				ss << L"Screenshot" << std::setfill(L'0') << std::setw(4) << nr++ << L".png";

                ID3D11Resource* pTex2D = nullptr;
                DXUTGetD3D11RenderTargetView()->GetResource(&pTex2D);
                SaveWICTextureToFile(DXUTGetD3D11DeviceContext(), pTex2D, GUID_ContainerFormatPng, ss.str().c_str());
                SAFE_RELEASE(pTex2D);

                std::wcout << L"Screenshot written to " << ss.str() << std::endl;
				break;
			}
            // F10: Toggle video recording
            case VK_F10:
            {
                if (!g_pFFmpegVideoRecorder) {
                    g_pFFmpegVideoRecorder = new FFmpeg(25, 21, FFmpeg::MODE_INTERPOLATE);
                    V(g_pFFmpegVideoRecorder->StartRecording(DXUTGetD3D11Device(), DXUTGetD3D11RenderTargetView(), "output.avi"));
                } else {
                    g_pFFmpegVideoRecorder->StopRecording();
                    SAFE_DELETE(g_pFFmpegVideoRecorder);
                }
				break;
            }	
#ifdef RIGID_BODY_SYSTEM
			case VK_UP:
			{
				if (g_iTestCase == 1 || g_iTestCase == 3)
					rigidBoxSystem.boxes[0].applyForce(0.0f, 0.0f, 5.0f);
			}
				break;
			case VK_DOWN:
			{
				if (g_iTestCase == 1 || g_iTestCase == 3)
					rigidBoxSystem.boxes[0].applyForce(0.0f, 0.0f, -5.0f);
			}
				break;
			case VK_LEFT:
			{
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					XMFLOAT3 force;
					for (int i = 0; i < rigidBoxSystem.boxes.size(); i++)
					{
						XMStoreFloat3(&force, XMVectorNegate(XMLoadFloat3(&rigidBoxSystem.boxes[i].getPosition())));
						rigidBoxSystem.boxes[i].applyForce(force);
					}
				}
				break;
			}
			case VK_RIGHT:
				if (g_iTestCase == 1 || g_iTestCase == 3)
				{
					for (int i = 0; i < rigidBoxSystem.boxes.size(); i++)
						rigidBoxSystem.boxes[i].applyForce(rigidBoxSystem.boxes[i].getPosition());
				}
			break;
#endif

		}
	}
}


//--------------------------------------------------------------------------------------
// Handle mouse button presses
//--------------------------------------------------------------------------------------
void CALLBACK OnMouse( bool bLeftButtonDown, bool bRightButtonDown, bool bMiddleButtonDown,
                       bool bSideButton1Down, bool bSideButton2Down, int nMouseWheelDelta,
                       int xPos, int yPos, void* pUserContext )
{

		static int xPosSave = 0, yPosSave = 0;

		if (bLeftButtonDown)
		{
			// Accumulate deltas in g_viMouseDelta
			g_viMouseDelta.x += xPos - xPosSave;
			g_viMouseDelta.y += yPos - yPosSave;
		}

		xPosSave = xPos;
		yPosSave = yPos;
}


//--------------------------------------------------------------------------------------
// Handle messages to the application
//--------------------------------------------------------------------------------------
LRESULT CALLBACK MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam,
                          bool* pbNoFurtherProcessing, void* pUserContext )
{
    // Send message to AntTweakbar first
    if (TwEventWin(hWnd, uMsg, wParam, lParam))
    {
        *pbNoFurtherProcessing = true;
        return 0;
    }

    // If message not processed yet, send to camera
	if(g_camera.HandleMessages(hWnd,uMsg,wParam,lParam))
    {
        *pbNoFurtherProcessing = true;
		return 0;
    }

	return 0;
}


//--------------------------------------------------------------------------------------
// Handle updates to the scene
//--------------------------------------------------------------------------------------
void CALLBACK OnFrameMove(double dTime, float fElapsedTime, void* pUserContext)
{
	UpdateWindowTitle(L"Demo");

	// Move camera
	g_camera.FrameMove(fElapsedTime);

	// Update effects with new view + proj transformations
	g_pEffectPositionColor->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionColor->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormal->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionNormal->SetProjection(g_camera.GetProjMatrix());

	g_pEffectPositionNormalColor->SetView(g_camera.GetViewMatrix());
	g_pEffectPositionNormalColor->SetProjection(g_camera.GetProjMatrix());

#ifdef MASS_SPRING_SYSTEM

	if (g_iPreTestCase != g_iTestCase){// test case changed
		// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);

		//g_fStepSize = 0.005f;
		//g_fTimeScale = 1;
		g_bDrawPoints = true;
		g_bDrawSprings = true;
		g_bDrawVelocities = false;

		g_vfMovableObjectPos = XMFLOAT3(0, 0, 0);
		g_vfRotate = XMFLOAT3(0, 0, 0);

		switch (g_iTestCase)
		{
		case 0:
			cout << "Console: output 1 step\n";
			massSpringSystem.points = scene1Points;
			massSpringSystem.springs = scene1Springs;
			massSpringSystem.useGravity = false;
			g_bSimulateByStep = true;
			massSpringSystem.Step(0.1f);
			cout << "**********************EULER***********************\n";
			cout << "----------------Point 1----------------\n";
			cout << massSpringSystem.points[0];
			cout << "----------------Point 2----------------\n";
			cout << massSpringSystem.points[1];

			massSpringSystem.points = scene1Points;
			massSpringSystem.springs = scene1Springs;
			massSpringSystem.integrator = Midpoint;
			massSpringSystem.Step(0.1f);
			cout << "**********************MIDPOINT***********************\n";
			cout << "----------------Point 1----------------\n";
			cout << massSpringSystem.points[0];
			cout << "----------------Point 2----------------\n";
			cout << massSpringSystem.points[1];
			break;
		case 1:
			cout << "Scene: 2 Points\n";

			massSpringSystem.points = std::vector<MassPoint>(scene1Points);
			massSpringSystem.springs = std::vector<Spring>(scene1Springs);
			massSpringSystem.useGravity = false;
			g_bSimulateByStep = false;
			break;
		case 2:
			cout << "Scene: Cube\n";

			massSpringSystem.points = std::vector<MassPoint>(scene2Points);
			massSpringSystem.springs = std::vector<Spring>(scene2Springs);
			massSpringSystem.useGravity = true;
			g_bSimulateByStep = false;
			break;
		default:
			cout << "Empty Test!\n";
			break;
		}
		g_iPreTestCase = g_iTestCase;
	}

	// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
	if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
	{
		// Calcuate camera directions in world space
		XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
		XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
		XMVECTOR camUpWorld = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

		// Add accumulated mouse deltas to movable object pos
		XMVECTOR vMovableObjectPos = XMLoadFloat3(&g_vfMovableObjectPos);

		float speedScale = 0.001f;
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos, speedScale * (float)g_viMouseDelta.x * camRightWorld);
		vMovableObjectPos = XMVectorAdd(vMovableObjectPos, -speedScale * (float)g_viMouseDelta.y * camUpWorld);

		speedScale = 0.1f;
		XMVECTOR vMovableObjectVel = speedScale * (float)g_viMouseDelta.x * camRightWorld;
		vMovableObjectVel = XMVectorAdd(vMovableObjectVel, -speedScale * (float)g_viMouseDelta.y * camUpWorld);;

		XMStoreFloat3(&g_vfMovableObjectPos, vMovableObjectPos);
		XMStoreFloat3(&g_vfMovableObjectVel, vMovableObjectVel);

		// Reset accumulated mouse deltas
		g_viMouseDelta = XMINT2(0, 0);
	}
	else
	{
		g_vfMovableObjectVel = XMFLOAT3(0, 0, 0);
	}


	XMVECTOR objectVelocity = XMLoadFloat3(&massSpringSystem.points[0].velocity);
	XMVECTOR mouseVelovity = XMLoadFloat3(&g_vfMovableObjectVel);
	objectVelocity = XMVectorAdd(objectVelocity, mouseVelovity);
	XMStoreFloat3(&massSpringSystem.points[0].velocity, objectVelocity);


	if (g_bSimulateByStep) lastStep = dTime;

	if (g_bSimulateByStep && DXUTIsKeyDown(VK_SPACE)){
		g_bIsSpaceReleased = false;
	}
	if (g_bSimulateByStep && !g_bIsSpaceReleased)
		if (DXUTIsKeyDown(VK_SPACE))
			return;
	if (g_bSimulateByStep && g_bIsSpaceReleased)
		return;


	int numberOfSteps = std::lroundf((float)(dTime - lastStep) / g_fStepSize);
	if (g_bSimulateByStep) numberOfSteps = 1;
	if (numberOfSteps != 0) lastStep = dTime;

	for (int i = 0; i < numberOfSteps; i++)
	{
		massSpringSystem.Step(g_fStepSize * g_fTimeScale);
	}


	if (g_bSimulateByStep)
		g_bIsSpaceReleased = true;
#endif
#ifdef RIGID_BODY_SYSTEM

	if (g_iPreTestCase != g_iTestCase) {// test case changed
										// clear old setup and build up new setup
		TwDeleteBar(g_pTweakBar);
		g_pTweakBar = nullptr;
		InitTweakBar(g_pPd3Device);

		g_vfMovableObjectPos = XMFLOAT3(0, 0, 0);
		g_vfRotate = XMFLOAT3(0, 0, 0);
		g_bSimulateByStep = false;

		switch (g_iTestCase)
		{
		case 0:
		{
			rigidBoxSystem.boxes = { box1 };
			rigidBoxSystem.boxes[0].applyForce(1.0f, 1.0f, 0.0f, 0.3f, 0.5f, 0.25f);
			rigidBoxSystem.doStep(2);
			cout << rigidBoxSystem.boxes[0];
			XMFLOAT3 worldPosition = rigidBoxSystem.boxes[0].getWorldPosition(-0.3f, -0.5f, -0.25f);
			cout << std::setw(25) << "World Position of Pi:" <<"(" << worldPosition.x << ", "
				<< worldPosition.y << ", " << worldPosition.z << ")\n";
			XMFLOAT3 worldVelocity = rigidBoxSystem.boxes[0].getWorldVelocity(-0.3f, -0.5f, -0.25f);
			cout << std::setw(25) << "World velocity of Pi:" << "(" << worldVelocity.x << ", "
				<< worldVelocity.y << ", " << worldVelocity.z << ")\n";
		}
			break;
		case 1:
			g_fStepSize = 0.01f;
			rigidBoxSystem.boxes = { box1 };
			rigidBoxSystem.boxes[0].applyForce(1.0f, 1.0f, 0.0f, 0.3f, 0.5f, 0.25f);
			g_bSimulateByStep = false;
			break;
		case 2:
			rigidBoxSystem.boxes = { box2, box3 };
			rigidBoxSystem.boxes[0].applyForce(-1.0f / g_fStepSize, 0.0f, 0.0f);
			rigidBoxSystem.boxes[1].applyForce(1.0f / g_fStepSize, 0.0f, 0.0f);
			g_bSimulateByStep = false;
			break;
		case 3:
			boxFloor.visible = false;
			rigidBoxSystem.boxes = { box2, box3, box4, box5, boxFloor };
			rigidBoxSystem.boxes[0].applyForce(-2.0f / g_fStepSize, 0.0f, 0.0f);
			rigidBoxSystem.boxes[1].applyForce(2.0f / g_fStepSize, 0.0f, 0.0f);
			rigidBoxSystem.boxes[2].applyForce(-1.0f / g_fStepSize, 0.0f, 0.0f);
			rigidBoxSystem.boxes[3].applyForce(0.0f, -1.0f / g_fStepSize, 0.0f);
			g_bSimulateByStep = false;
			break;
		default:
			cout << "Empty Test!\n";
			break;
		}
		g_iPreTestCase = g_iTestCase;
	}

	if (g_iTestCase == 0)
	{
		lastStep = dTime;
		return;
	}
	else if (g_iTestCase == 1 || g_iTestCase == 3)
	{
		// Apply accumulated mouse deltas to g_vfMovableObjectPos (move along cameras view plane)
		if (g_viMouseDelta.x != 0 || g_viMouseDelta.y != 0)
		{
			// Calcuate camera directions in world space
			XMMATRIX viewInv = XMMatrixInverse(nullptr, g_camera.GetViewMatrix());
			XMVECTOR camRightWorld = XMVector3TransformNormal(g_XMIdentityR0, viewInv);
			XMVECTOR camUpWorld = XMVector3TransformNormal(g_XMIdentityR1, viewInv);

			float speedScale = 1.0f;
			XMVECTOR vMovableObjectVel = speedScale * (float)g_viMouseDelta.x * camRightWorld;
			vMovableObjectVel = XMVectorAdd(vMovableObjectVel, -speedScale * (float)g_viMouseDelta.y * camUpWorld);;
			DirectX::XMStoreFloat3(&g_vfMovableObjectVel, vMovableObjectVel);

			// Reset accumulated mouse deltas
			g_viMouseDelta = XMINT2(0, 0);
		}
		else
		{
			g_vfMovableObjectVel = XMFLOAT3(0, 0, 0);
		}


		XMFLOAT3 mouseForce = g_vfMovableObjectVel;
		rigidBoxSystem.boxes[0].applyForce(mouseForce);
	}

	if (g_bSimulateByStep) lastStep = dTime;

	if (g_bSimulateByStep && DXUTIsKeyDown(VK_SPACE)) {
		g_bIsSpaceReleased = false;
	}
	if (g_bSimulateByStep && !g_bIsSpaceReleased)
		if (DXUTIsKeyDown(VK_SPACE))
			return;
	if (g_bSimulateByStep && g_bIsSpaceReleased)
		return;


	int numberOfSteps = std::lroundf((float)(dTime - lastStep) / g_fStepSize);
	if (g_bSimulateByStep) numberOfSteps = 1;
	if (numberOfSteps != 0) lastStep = dTime;

	for (int i = 0; i < numberOfSteps; i++)
		rigidBoxSystem.doStep(g_fStepSize * g_fTimeScale);


	if (g_bSimulateByStep)
		g_bIsSpaceReleased = true;
#endif
}

//--------------------------------------------------------------------------------------
// Render the scene using the D3D11 device
//--------------------------------------------------------------------------------------
void CALLBACK OnD3D11FrameRender( ID3D11Device* pd3dDevice, ID3D11DeviceContext* pd3dImmediateContext,
                                  double fTime, float fElapsedTime, void* pUserContext )
{
    HRESULT hr;

	// Clear render target and depth stencil
	float ClearColor[4] = { 0.0f, 0.0f, 0.0f, 0.0f };

	ID3D11RenderTargetView* pRTV = DXUTGetD3D11RenderTargetView();
	ID3D11DepthStencilView* pDSV = DXUTGetD3D11DepthStencilView();
	pd3dImmediateContext->ClearRenderTargetView( pRTV, ClearColor );
	pd3dImmediateContext->ClearDepthStencilView( pDSV, D3D11_CLEAR_DEPTH, 1.0f, 0 );

    // Draw floor
    DrawFloor(pd3dImmediateContext);

    // Draw axis box
    //DrawBoundingBox(pd3dImmediateContext);
    
#ifdef MASS_SPRING_SYSTEM
	DrawMassSpringSystem(pd3dImmediateContext);
#endif
#ifdef RIGID_BODY_SYSTEM
	DrawRigidBodySystem(pd3dImmediateContext);
#endif

    // Draw GUI
    TwDraw();

    if (g_pFFmpegVideoRecorder) 
    {
        V(g_pFFmpegVideoRecorder->AddFrame(pd3dImmediateContext, DXUTGetD3D11RenderTargetView()));
    }
}

//--------------------------------------------------------------------------------------
// Initialize everything and go into a render loop
//--------------------------------------------------------------------------------------
int main(int argc, char* argv[])
{
#if defined(DEBUG) | defined(_DEBUG)
	// Enable run-time memory check for debug builds.
	// (on program exit, memory leaks are printed to Visual Studio's Output console)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
#endif

#ifdef _DEBUG
	std::wcout << L"---- DEBUG BUILD ----\n\n";
#endif

	// Set general DXUT callbacks
	DXUTSetCallbackMsgProc( MsgProc );
	DXUTSetCallbackMouse( OnMouse, true );
	DXUTSetCallbackKeyboard( OnKeyboard );

	DXUTSetCallbackFrameMove( OnFrameMove );
	DXUTSetCallbackDeviceChanging( ModifyDeviceSettings );

	// Set the D3D11 DXUT callbacks
	DXUTSetCallbackD3D11DeviceAcceptable( IsD3D11DeviceAcceptable );
	DXUTSetCallbackD3D11DeviceCreated( OnD3D11CreateDevice );
	DXUTSetCallbackD3D11SwapChainResized( OnD3D11ResizedSwapChain );
	DXUTSetCallbackD3D11FrameRender( OnD3D11FrameRender );
	DXUTSetCallbackD3D11SwapChainReleasing( OnD3D11ReleasingSwapChain );
	DXUTSetCallbackD3D11DeviceDestroyed( OnD3D11DestroyDevice );

    // Init camera
 	XMFLOAT3 eye(0.0f, 0.0f, -2.0f);
	XMFLOAT3 lookAt(0.0f, 0.0f, 0.0f);
	g_camera.SetViewParams(XMLoadFloat3(&eye), XMLoadFloat3(&lookAt));
    g_camera.SetButtonMasks(MOUSE_MIDDLE_BUTTON, MOUSE_WHEEL, MOUSE_RIGHT_BUTTON);

    // Init DXUT and create device
	DXUTInit( true, true, NULL ); // Parse the command line, show msgboxes on error, no extra command line params
	//DXUTSetIsInGammaCorrectMode( false ); // true by default (SRGB backbuffer), disable to force a RGB backbuffer
	DXUTSetCursorSettings( true, true ); // Show the cursor and clip it when in full screen
	DXUTCreateWindow( L"Demo" );
	DXUTCreateDevice( D3D_FEATURE_LEVEL_11_0, true, 1280, 960 );
    
	DXUTMainLoop(); // Enter into the DXUT render loop

	DXUTShutdown(); // Shuts down DXUT (includes calls to OnD3D11ReleasingSwapChain() and OnD3D11DestroyDevice())
	
	return DXUTGetExitCode();
}
