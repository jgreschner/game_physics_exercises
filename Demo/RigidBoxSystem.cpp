#include "RigidBoxSystem.h"


RigidBoxSystem::RigidBoxSystem()
{
	
}


RigidBoxSystem::~RigidBoxSystem()
{
}

void RigidBoxSystem::doStep(float delta)
{
	//Move boxes
	for (int i = 0; i < boxes.size(); i++)
	{
		if(useGravity)
			boxes[i].applyForce(0.0f, gravity * boxes[i].mass, 0.0f);
		boxes[i].doStep(delta);
	}

	//Check collision
	for (int i = 0; i < boxes.size(); i++)
	{
		for (int j = i + 1; j < boxes.size(); j++)
		{
			// Check if a corner of j is in i
			CollisionInfo collisionTest = checkCollision(XMLoadFloat4x4(&boxes[i].transformMatrix()), 
														    XMLoadFloat4x4(&boxes[j].transformMatrix()));
			if (!collisionTest.isValid) 
			{
				// Check if a corner of i is in j
				collisionTest = checkCollision(XMLoadFloat4x4(&boxes[j].transformMatrix()),
												XMLoadFloat4x4(&boxes[i].transformMatrix()));
				collisionTest.normalWorld = -collisionTest.normalWorld;// we compute the impulse to A
			}

			if (collisionTest.isValid)
			{	
				//Check if colliding, separating or sliding
				XMFLOAT3 impulsePosition;
				XMStoreFloat3(&impulsePosition, collisionTest.collisionPointWorld);
				XMFLOAT3 tmpRelVel;
				XMStoreFloat3(&tmpRelVel, XMVector3Dot(XMLoadFloat3(&boxes[i].getWorldVelocity(impulsePosition)) -
					XMLoadFloat3(&boxes[j].getWorldVelocity(impulsePosition)), collisionTest.normalWorld));
				float relVelDotNormal = tmpRelVel.x;
				//Stop if separating or sliding
				if (relVelDotNormal >= 0.0f)
					continue;

				//Compute impulse
				XMFLOAT3 impulseForce;
				XMStoreFloat3(&impulseForce, XMVectorScale(collisionTest.normalWorld, 
					computeImpulse(boxes[i], boxes[j], collisionTest, relVelDotNormal)));

				//Apply impulse
				applyImpulse(boxes[i], impulseForce, impulsePosition);
				XMStoreFloat3(&impulseForce, XMVectorNegate(XMLoadFloat3(&impulseForce)));
				applyImpulse(boxes[j], impulseForce, impulsePosition);
			}
		}

		//Check collision with the floor
		
	}
}

/*
* Comment:
*	Calculates the impulse between two rigid boxes for a given collision.
* Formula : 
*	J = (-(1+c) * dot(Vr,n)) / ( 1/Ma + 1/Mb + dot( [( (Inv(Ia)*(Xa x n) ) x Xa + ((Inv(Ib)*(Xb x n)) x Xb], n) )
* Note:	
*	J  : impulse force
*	c  : restitution coefficient
*	Vr : relative velocity between the two rigid boxes(Va - Vb)
*	n  : collision normal
*	M  : mass of the rigid box
*	I  : inertia tensor of the rigid box
*	X  : world position of the rigid box
*/
float RigidBoxSystem::computeImpulse(const RigidBox& a, const RigidBox& b, const CollisionInfo& info, float relVelDotNormal) const
{
	XMVECTOR x1 = XMLoadFloat3(&a.position);
	XMVECTOR x2 = XMLoadFloat3(&b.position);
	XMMATRIX inverseI1 = XMLoadFloat3x3(&a.inertiaTensorInverse);
	XMMATRIX inverseI2 = XMLoadFloat3x3(&b.inertiaTensorInverse);

	XMFLOAT3 dotProduct;
	XMStoreFloat3(&dotProduct, XMVector3Dot(XMVector3Cross(XMVector3Transform(
		XMVector3Cross(x1, info.normalWorld), inverseI1), x1) 
		+ XMVector3Cross(XMVector3Transform(XMVector3Cross(
		x2, info.normalWorld), inverseI2), x2), info.normalWorld));

	return  (-(1 + bounciness) * relVelDotNormal) / (a.massInverse + b.massInverse + dotProduct.x);
}

/*
* Comment:
*	Applies the impulse to a rigid box's velocity and angular 
*	momentum for a given impulse force and position.
* Formula : 
*	v' = v + Jn/M
*	L' = L + (X x Jn)
* Note:
*	v  : velocity of the rigid box
*	Jn : impulse force in the normal direction
*	M  : mass of the rigid box
*	L  : angular momentum of the rigid box
*	X  : position of the impulse relative to the
*		 center of mass of the rigid box
*/
void RigidBoxSystem::applyImpulse(RigidBox& b, const XMFLOAT3& impulseForces, const XMFLOAT3& impulsePosition)
{
	if (b.isFixed)
		return;

	//Offset velocity
	XMStoreFloat3(&b.velocity, XMVectorAdd(XMLoadFloat3(&b.velocity), XMVectorScale(XMLoadFloat3(&impulseForces), b.massInverse)));
	//Offset angular momentum
	XMStoreFloat3(&b.angularMomentum, XMVectorAdd(XMLoadFloat3(&b.angularMomentum),
		XMVector3Cross(XMLoadFloat3(&impulsePosition) - XMLoadFloat3(&b.position), XMLoadFloat3(&impulseForces))));
}
