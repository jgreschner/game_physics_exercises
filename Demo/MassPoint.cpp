#include "MassPoint.h"
#include <iomanip>

MassPoint::MassPoint(const XMFLOAT3& _pos, float _mass, bool _isFixed)
	: position(_pos)
	, velocity(0, 0, 0)
	, force(0, 0, 0)
	, mass(_mass)
	, isFixed(_isFixed)
{
}

MassPoint::MassPoint(float _x, float _y, float _z, float _mass, bool _isFixed)
	: position(_x, _y, _z)
	, velocity(0, 0, 0)
	, force(0, 0, 0)
	, mass(_mass)
	, isFixed(_isFixed)
{
}

MassPoint::MassPoint(const XMFLOAT3& _pos, const XMFLOAT3& _vel, float _mass)
	: position(_pos)
	, velocity(_vel)
	, force(0, 0, 0)
	, mass(_mass)
	, isFixed(false)
{
}

MassPoint::~MassPoint()
{
}

XMFLOAT3 MassPoint::acceleration() const
{
	XMVECTOR tmpAcceleration = XMLoadFloat3(&force);
	tmpAcceleration = XMVectorScale(tmpAcceleration, 1.0f / mass);
	XMFLOAT3 acceleration;
	XMStoreFloat3(&acceleration, tmpAcceleration);

	return acceleration;
}

std::ostream& operator<< (std::ostream& stream, const MassPoint& MassPoint)
{
	stream << std::setw(10) << "Position:" << "(" << MassPoint.position.x << ", "
		<< MassPoint.position.y << ", " << MassPoint.position.z << ")" << std::endl;
	stream << std::setw(10) << "Velocity:" << "(" << MassPoint.velocity.x << ", "
		<< MassPoint.velocity.y << ", " << MassPoint.velocity.z << ")" << std::endl;
	stream << std::setw(10) << "Force:" << "(" << MassPoint.force.x << ", "
		<< MassPoint.force.y << ", " << MassPoint.force.z << ")" << std::endl;
	stream << std::setw(10) << "Mass:" << MassPoint.mass << std::endl;
	return stream;
}
