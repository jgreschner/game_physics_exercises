#include "RigidBox.h"



RigidBox::RigidBox(float _mass, float _posX, float _posY, float _posZ,
	float _sizeX, float _sizeY, float _sizeZ,
	float _angleX, float _angleY, float _angleZ)
	: RigidBody(_mass, _posX, _posY, _posZ, _sizeX, _sizeY, _sizeZ, _angleX, _angleY, _angleZ)
{
	if(!isFixed)
		initializeInertiaTensorInverse();
}

RigidBox::~RigidBox()
{
}

void RigidBox::initializeInertiaTensorInverse()
{
	float inertiaTensorX = (mass / 12) * (size.y*size.y + size.z*size.z);
	float inertiaTensorY = (mass / 12) * (size.x*size.x + size.z*size.z);
	float inertiaTensorZ = (mass / 12) * (size.x*size.x + size.y*size.y);

	XMFLOAT3X3 inertiaTensor = XMFLOAT3X3(inertiaTensorX, 0.0f, 0.0f,
											0.0f, inertiaTensorY, 0.0f,
											0.0f, 0.0f, inertiaTensorZ);

	XMStoreFloat3x3(&inertiaTensorInverse, XMMatrixInverse(nullptr, XMLoadFloat3x3(&inertiaTensor)));
}
