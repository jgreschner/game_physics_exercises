
#include <vector>

#include "MassPoint.h"
#include "Spring.h"

#ifndef _MASS_SPRING_SYSTEM_H_
#define _MASS_SPRING_SYSTEM_H_

enum Integrator {
	Euler, 
	Midpoint, 
	Leap_Frog 
};

class MassSpringSystem
{
public:
	MassSpringSystem();
	~MassSpringSystem();

	void Step(float delta);

	float gravity = -9.81f;
	float floorHeight = -1;
	float floorFriction = 0.01f;
	float bounciness = 0.3f;
	bool useGravity = false;
	Integrator integrator = Euler;
	std::vector<MassPoint> points;
	std::vector<Spring> springs;
private:
	static void integrate(float _timeStep, XMFLOAT3& _x, const XMFLOAT3& _dx);

	void computeCollision(MassPoint& point);
	void computeElasticForces(Spring& s);
	void computeElasticForces(Spring& s, MassPoint& point1, MassPoint& point2);
};

#endif//_MASS_SPRING_SYSTEM_H_