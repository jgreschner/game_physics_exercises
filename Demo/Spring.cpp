#include "Spring.h"

Spring::Spring(int _p1, int _p2, float _stiff, float _initLength)
	: massPoint1(_p1), massPoint2(_p2), stiffness(_stiff), initialLength(_initLength)
{
}

Spring::~Spring()
{
}