#include "RigidBody.h"
#include <iomanip>


RigidBody::RigidBody()
{
}

RigidBody::RigidBody(float _mass, float _posX, float _posY, float _posZ, 
	float _sizeX, float _sizeY, float _sizeZ,
	float _angleX, float _angleY, float _angleZ)
	: position(_posX, _posY, _posZ)
	, size(_sizeX, _sizeY, _sizeZ)
	, mass(_mass)
	, velocity(0.0f, 0.0f, 0.0f)
	, angularVelocity(0.0f, 0.0f, 0.0f)
	, forceAccumulator(0.0f, 0.0f, 0.0f)
	, torqueAccumulator(0.0f, 0.0f, 0.0f)
	, angularMomentum(0.0f, 0.0f, 0.0f)
{
	if (mass != 0.0f)
		massInverse = 1.0f / mass;
	else
	{
		isFixed = true;
		massInverse = 0.0f;
		inertiaTensorInverse = XMFLOAT3X3(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f);
	}
	XMStoreFloat4(&orientation, XMQuaternionRotationRollPitchYaw(_angleX, _angleY, _angleZ));
	XMMATRIX rotation = XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));
	XMMATRIX scale = XMMatrixScaling(size.x, size.y, size.z);
	XMMATRIX translation = XMMatrixTranslation(position.x, position.y, position.z);
	XMStoreFloat4x4(&transform, scale * rotation * translation);
}

RigidBody::~RigidBody()
{
}

XMFLOAT3 RigidBody::acceleration() const
{
	XMVECTOR tmpAcceleration = XMLoadFloat3(&forceAccumulator);
	tmpAcceleration = XMVectorScale(tmpAcceleration, massInverse);
	XMFLOAT3 acceleration;
	XMStoreFloat3(&acceleration, tmpAcceleration);

	return acceleration;
}

XMFLOAT3 RigidBody::getWorldPosition(float _relativeX, float _relativeY, float _relativeZ) const
{
	XMFLOAT3 worldPosition;
	XMStoreFloat3(&worldPosition, XMVector3Transform(XMLoadFloat3(
		&XMFLOAT3(_relativeX, _relativeY, _relativeZ)), XMMatrixRotationQuaternion(XMLoadFloat4(&orientation))));
	XMStoreFloat3(&worldPosition, XMVectorAdd(XMLoadFloat3(&position), XMLoadFloat3(&worldPosition)));
	return worldPosition;
}

XMFLOAT3 RigidBody::getWorldVelocity(float _relativeX, float _relativeY, float _relativeZ) const
{
	XMFLOAT3 worldVelocity;
	XMStoreFloat3(&worldVelocity, XMVector3Cross(XMLoadFloat3(&angularVelocity), 
		XMLoadFloat3(&XMFLOAT3(_relativeX, _relativeY, _relativeZ))));
	XMStoreFloat3(&worldVelocity, XMVectorAdd(XMLoadFloat3(&velocity), XMLoadFloat3(&worldVelocity)));
	return worldVelocity;
}

XMFLOAT3 RigidBody::getWorldVelocity(const XMFLOAT3& _worldPosition) const
{
	XMFLOAT3 relativePosition;
	XMStoreFloat3(&relativePosition, XMLoadFloat3(&_worldPosition) - XMLoadFloat3(&position));
	return getWorldVelocity(relativePosition.x, relativePosition.y, relativePosition.z);
}

void RigidBody::applyForce(const XMFLOAT3& _force)
{
	//Add force to the force accumulator
	forceAccumulator.x += _force.x;
	forceAccumulator.y += _force.y;
	forceAccumulator.z += _force.z;

}

void RigidBody::applyForce(const XMFLOAT3& _force, const XMFLOAT3& _worldPosition)
{
	//Apply force at the center of mass
	applyForce(_force);

	//Add the new torque
	XMVECTOR positionApplied = XMLoadFloat3(&_worldPosition) - XMLoadFloat3(&position);
	XMVECTOR newTorque = XMVector3Cross(positionApplied, XMLoadFloat3(&_force));
	newTorque = XMVectorAdd(XMLoadFloat3(&torqueAccumulator), newTorque);
	XMStoreFloat3(&torqueAccumulator, newTorque);
}

void RigidBody::applyForce(float _forceX, float _forceY, float _forceZ)
{
	//Add force to the force accumulator
	XMFLOAT3 force(_forceX, _forceY, _forceZ);
	applyForce(force);
}

void RigidBody::applyForce(float _forceX, float _forceY, float _forceZ,
	float _posX, float _posY, float _posZ)
{
	//Add force to the force accumulator
	XMFLOAT3 force(_forceX, _forceY, _forceZ);
	XMFLOAT3 position(_posX, _posY, _posZ);
	applyForce(force, position);
}

void RigidBody::clearForcesAndTorque()
{
	//Clear forces
	forceAccumulator.x = 0.0f;
	forceAccumulator.y = 0.0f;
	forceAccumulator.z = 0.0f;
	//Clear torque
	torqueAccumulator.x = 0.0f;
	torqueAccumulator.y = 0.0f;
	torqueAccumulator.z = 0.0f;
}

void RigidBody::doStep(float _delta)
{
	if (!isFixed)
	{
		//Integrate position
		integrate(_delta, position, velocity);

		//Integrate velocity
		integrate(_delta, velocity, acceleration());

		//Integrate orientation
		integrate(_delta / 2.0f, orientation, XMFLOAT4(angularVelocity.x,
			angularVelocity.y, angularVelocity.z, 0.0f));
		XMStoreFloat4(&orientation, XMQuaternionNormalize(XMLoadFloat4(&orientation)));

		//Integrate angular momentum
		integrate(_delta, angularMomentum, torqueAccumulator);

		//Compute current inertia tensor
		XMMATRIX rotation = XMMatrixRotationQuaternion(XMLoadFloat4(&orientation));
		XMMATRIX inertiaTensor = XMMatrixMultiply(rotation, XMLoadFloat3x3(&inertiaTensorInverse));
		inertiaTensor = XMMatrixMultiply(inertiaTensor, XMMatrixTranspose(rotation));

		//Compute angular velocity
		XMStoreFloat3(&angularVelocity, XMVector3Transform(XMLoadFloat3(&angularMomentum), inertiaTensor));

		//Update transform matrix
		XMMATRIX scale = XMMatrixScaling(size.x, size.y, size.z);
		XMMATRIX translation = XMMatrixTranslation(position.x, position.y, position.z);
		XMStoreFloat4x4(&transform, scale * rotation * translation);

		//Clear forces and torque
		clearForcesAndTorque();
	}
}

void RigidBody::integrate(float _timeStep, XMFLOAT3& _x, const XMFLOAT3& _dx)
{
	XMVECTOR newValue = XMLoadFloat3(&_dx);
	newValue = XMVectorScale(newValue, _timeStep);
	newValue = XMVectorAdd(XMLoadFloat3(&_x), newValue);
	XMStoreFloat3(&_x, newValue);
}

void RigidBody::integrate(float _timeStep, XMFLOAT4& _x, const XMFLOAT4& _dx)
{
	XMVECTOR newValue = XMLoadFloat4(&_dx);
	newValue = XMVectorScale(newValue, _timeStep);
	newValue = XMVectorAdd(XMLoadFloat4(&_x), newValue);
	XMStoreFloat4(&_x, newValue);
}

std::ostream& operator<< (std::ostream& stream, const RigidBody& rb)
{
	stream << std::setw(25) << "Mass:" << rb.mass << std::endl;
	stream << std::setw(25) << "Position:" << "(" << rb.position.x << ", "
		<< rb.position.y << ", " << rb.position.z << ")" << std::endl;
	stream << std::setw(25) << "Velocity:" << "(" << rb.velocity.x << ", "
		<< rb.velocity.y << ", " << rb.velocity.z << ")" << std::endl;
	stream << std::setw(25) << "Orientation:" << "(" << rb.orientation.x << ", "
		<< rb.orientation.y << ", " << rb.orientation.z << ", " << rb.orientation.w << ")" << std::endl;
	stream << std::setw(25) << "Angular momentum:" << "(" << rb.angularMomentum.x << ", "
		<< rb.angularMomentum.y << ", " << rb.angularMomentum.z << ")" << std::endl;
	stream << std::setw(25) << "Angular velocity:" << "(" << rb.angularVelocity.x << ", "
		<< rb.angularVelocity.y << ", " << rb.angularVelocity.z << ")" << std::endl;
	stream << std::setw(25) << "Inertia tensor inversed:" << "(" << 
		rb.inertiaTensorInverse._11 << " " << rb.inertiaTensorInverse._12 << " " << rb.inertiaTensorInverse._13 << std::endl << std::setw(26) << " " <<
		rb.inertiaTensorInverse._21 << " " << rb.inertiaTensorInverse._22 << " " << rb.inertiaTensorInverse._23 << std::endl << std::setw(26) << " " <<
		rb.inertiaTensorInverse._31 << " " << rb.inertiaTensorInverse._32 << " " << rb.inertiaTensorInverse._33 << ")" << std::endl;

	return stream;
}