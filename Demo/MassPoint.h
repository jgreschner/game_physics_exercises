
#ifndef _MassPoint_H_
#define _MassPoint_H_

#include <DirectXMath.h>
#include <iostream>

using namespace DirectX;

struct MassPoint
{
	MassPoint(const XMFLOAT3& _pos, float _mass, bool _isFixed = false);
	MassPoint(float _x, float _y, float _z, float _mass, bool _isFixed = false);
	MassPoint(const XMFLOAT3& _pos, const XMFLOAT3& _vel, float _mass);
	~MassPoint();

	XMFLOAT3 acceleration() const;

	friend std::ostream& operator<< (std::ostream& stream, const MassPoint& massPoint);

	XMFLOAT3 position;
	XMFLOAT3 velocity;
	XMFLOAT3 force;
	float mass;
	bool isFixed;
};

#endif //_MassPoint_H_