
#include <DirectXMath.h>
#include "MassSpringSystem.h"

using namespace DirectX;


MassSpringSystem::MassSpringSystem()
{
}


MassSpringSystem::~MassSpringSystem()
{
}

void MassSpringSystem::Step(float delta) {

	//Clear forces and add gravity
	for (int i = 0; i < points.size(); i++)
	{
		if (useGravity)
			points[i].force = XMFLOAT3(0, gravity * points[i].mass, 0);
		else
			points[i].force = XMFLOAT3(0, 0, 0);
	}

	switch (integrator)
	{
	case(Euler) :
			//Add elastic forces
			for (int i = 0; i < springs.size(); i++)
				computeElasticForces(springs[i]);
			
			//Integrate position and velocity
			for (int i = 0; i < points.size(); i++)
			{
				if (points[i].isFixed)
					continue;

				//Integrate position
				integrate(delta, points[i].position, points[i].velocity);
				//Integrate velocity
				integrate(delta, points[i].velocity, points[i].acceleration());

				//Check for collisions with the floor
				computeCollision(points[i]);
			}
			break;
		case(Midpoint) :
		{
				//Create mid-point for every mass point
				std::vector<MassPoint> midPoints = points;

				//Compute mid-points positions at x(t + h/2)
				for (int i = 0; i < points.size(); i++)
				{
					if (points[i].isFixed)
						continue;
					integrate(delta / 2.0f, midPoints[i].position, points[i].velocity);
				}

				//Compute acceleration of the points
				for (int i = 0; i < springs.size(); i++)
					computeElasticForces(springs[i]);

				//Compute mid-points velocities at v(t + h/2) and compute points positions at x(t + h)
				for (int i = 0; i < points.size(); i++)
				{
					if (points[i].isFixed)
						continue;
					integrate(delta / 2.0f, midPoints[i].velocity, points[i].acceleration());
					integrate(delta, points[i].position, midPoints[i].velocity);
				}
				//Compute acceleration of the mid-points
				for (int i = 0; i < springs.size(); i++)
					computeElasticForces(springs[i], midPoints[springs[i].massPoint1], midPoints[springs[i].massPoint2]);

				//Compute points velocities at v(t + h)
				for (int i = 0; i < points.size(); i++)
				{
					if (points[i].isFixed)
						continue;
					integrate(delta, points[i].velocity, midPoints[i].acceleration());
					//Check for collision with the floor
					computeCollision(points[i]);
				}
			}
			break;
		case(Leap_Frog) :
			//Add elastic forces
			for (int i = 0; i < springs.size(); i++)
				computeElasticForces(springs[i]);

			//Integrate position and velocty
			for (int i = 0; i < points.size(); i++)
			{
				if (points[i].isFixed)
					continue;
				//Compute v(t - h/2)
				integrate(-delta/2.0f, points[i].velocity, points[i].acceleration());
				//Compute v(t + h/2)
				integrate(delta, points[i].velocity, points[i].acceleration());
				//Compute x(t + h)
				integrate(delta, points[i].position, points[i].velocity);
				//Check for collision with the floor
				computeCollision(points[i]);
			}
			break;
		default:
			break;
	}
}

void MassSpringSystem::integrate(float _timeStep, XMFLOAT3& _x, const XMFLOAT3& _dx)
{
	XMVECTOR newValue = XMLoadFloat3(&_dx);
	newValue = XMVectorScale(newValue, _timeStep);
	newValue = XMVectorAdd(XMLoadFloat3(&_x), newValue);
	XMStoreFloat3(&_x, newValue);
}

void MassSpringSystem::computeElasticForces(Spring& s) {
	computeElasticForces(s, points[s.massPoint1], points[s.massPoint2]);
}

void MassSpringSystem::computeElasticForces(Spring& s, MassPoint& point1, MassPoint& point2) {
	XMVECTOR posDiff = XMVectorSubtract(XMLoadFloat3(&point1.position), XMLoadFloat3(&point2.position));
	float currentLength = XMVectorGetX(XMVector3Length(posDiff));
	float fm = -s.stiffness * (currentLength - s.initialLength);
	XMVECTOR force = XMVectorScale(XMVector3Normalize(posDiff), fm);

	XMStoreFloat3(&point1.force, XMVectorAdd(XMLoadFloat3(&point1.force), force));
	XMStoreFloat3(&point2.force, XMVectorAdd(XMLoadFloat3(&point2.force), XMVectorNegate(force)));
}

void MassSpringSystem::computeCollision(MassPoint& point)
{
	if (point.position.y < floorHeight)
	{
		point.position.y = floorHeight;
		point.velocity = XMFLOAT3(point.velocity.x * (1 - floorFriction)
								  , point.velocity.y * -bounciness
								  , point.velocity.z * (1 - floorFriction));
	}
}