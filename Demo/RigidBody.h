#ifndef _ROGID_BODY_H_
#define _ROGID_BODY_H_

#include <DirectXMath.h>
#include <ostream>

using namespace DirectX;

class RigidBody
{
public:
	RigidBody();
	RigidBody(float _mass, float _posX, float _posY, float _posZ, 
		float _sizeX = 1.0f, float _sizeY = 1.0f, float _sizeZ = 1.0f,
		float _angleX = 0.0f, float _angleY = 0.0f, float _angleZ = 0.0f);
	~RigidBody();

	XMFLOAT3 acceleration() const;
	const XMFLOAT4X4& transformMatrix() const;
	const XMFLOAT3& getPosition() const;
	XMFLOAT3 getWorldPosition(float _relativeX, float _relativeY, float _relativeZ) const;
	XMFLOAT3 getWorldVelocity(float _relativeX, float _relativeY, float _relativeZ) const;
	XMFLOAT3 getWorldVelocity(const XMFLOAT3& _worldPosition) const;
	void applyForce(const XMFLOAT3& _force);
	void applyForce(const XMFLOAT3& _force, const XMFLOAT3& _worldPosition);
	void applyForce(float _forceX, float _forceY, float _forceZ);
	void applyForce(float _forceX, float _forceY, float _forceZ,
		float _posX, float _posY, float _posZ);

	void doStep(float _delta);

	friend std::ostream& operator<< (std::ostream& stream, const RigidBody& rb);

	bool visible = true;
protected:
	virtual void initializeInertiaTensorInverse() = 0;

	bool isFixed = false;
	float mass;
	float massInverse;
	XMFLOAT3 position;
	XMFLOAT4 orientation;
	XMFLOAT3 size;
	XMFLOAT3 velocity;
	XMFLOAT3X3 inertiaTensorInverse; 
	XMFLOAT3 angularVelocity;
	XMFLOAT3 forceAccumulator;
	XMFLOAT3 torqueAccumulator;
	XMFLOAT3 angularMomentum;
	XMFLOAT4X4 transform;
private:
	static void integrate(float _timeStep, XMFLOAT3& _x, const XMFLOAT3& _dx);
	static void integrate(float _timeStep, XMFLOAT4& _x, const XMFLOAT4& _dx);
	void clearForcesAndTorque();
};


inline const XMFLOAT4X4& RigidBody::transformMatrix() const
{
	return transform;
}

inline const XMFLOAT3& RigidBody::getPosition() const
{
	return position;
}

#endif //_ROGID_BODY_H_