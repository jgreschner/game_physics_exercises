#ifndef _RIGID_BOX_H_
#define _RIGID_BOX_H_

#include "RigidBody.h"

class RigidBox : public RigidBody
{
friend class RigidBoxSystem;
public:
	RigidBox(float _mass, float _posX, float _posY, float _posZ,
		float _sizeX, float _sizeY, float _sizeZ, float _angleX = 0.0f, 
		float _angleY = 0.0f, float _angleZ = 0.0f);
	~RigidBox();

private:
	virtual void initializeInertiaTensorInverse() override;
};

#endif//_RIGID_BOX_H_