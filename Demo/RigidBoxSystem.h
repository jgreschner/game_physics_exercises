
#ifndef _RIGID_BOX_SYSTEM_H_
#define _RIGID_BOX_SYSTEM_H_

#include <vector>
#include "RigidBox.h"
#include "collisionDetect.h"

class RigidBoxSystem
{
public:
	RigidBoxSystem();
	~RigidBoxSystem();

	void doStep(float delta);

	float gravity = -9.81f;
	bool useGravity = false;
	float bounciness = 0.3f;
	std::vector<RigidBox> boxes;
private:
	float computeImpulse(const RigidBox& b1, const RigidBox& b2, const CollisionInfo& info, float relVelDotNormal) const;
	void applyImpulse(RigidBox& b, const XMFLOAT3& impulseForces, const XMFLOAT3& impulsePosition);
};

#endif//_RIGID_BOX_SYSTEM_H_